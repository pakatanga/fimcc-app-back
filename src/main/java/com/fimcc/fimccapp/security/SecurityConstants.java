package com.fimcc.fimccapp.security;

public class SecurityConstants {
    public static final String SECRET = "SecretKeyToGenJWTs";
    public static final long EXPIRATION_TIME = 864_000_000;
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String USER_URL = "/user";
    public static final String CLAIMS_AUTHORITIES_KEY = "ROLES";
    public static final String EMPTY_STRING = "";
    public static final String CLAIMS_AUTHORITIES_SEPARATOR = ",";
    public static final String AUTHENTICATE_URL = "/authenticate";
}

