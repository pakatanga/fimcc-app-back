package com.fimcc.fimccapp.security;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.fimcc.fimccapp.service.jwt.JWTService;
import com.fimcc.fimccapp.utils.StringUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.fimcc.fimccapp.security.SecurityConstants.HEADER_STRING;
import static com.fimcc.fimccapp.security.SecurityConstants.TOKEN_PREFIX;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter   {

    public JWTAuthorizationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    @Override
    public void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        String token = req.getHeader(HEADER_STRING);

        if (StringUtils.isEmpty(token) || !StringUtils.startsWith(token, TOKEN_PREFIX)) {
            chain.doFilter(req, res);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(token);

        if (authentication == null) {
            chain.doFilter(req, res);
            return;
        }

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);

    }

    private UsernamePasswordAuthenticationToken getAuthentication(String token) {
        if (token != null) {
            DecodedJWT decodedToken = JWTService.extractDecodedToken(token);
            if (decodedToken != null) {
                return JWTService.createAuthenticationFromToken(decodedToken);
            }
            return null;
        }
        return null;
    }

}
