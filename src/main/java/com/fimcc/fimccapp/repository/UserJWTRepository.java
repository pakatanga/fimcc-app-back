package com.fimcc.fimccapp.repository;

import com.fimcc.fimccapp.security.UserJWT;
import org.springframework.data.repository.CrudRepository;

public interface UserJWTRepository extends CrudRepository<UserJWT, Long> {

    UserJWT findOneByUsername(String username);

}
