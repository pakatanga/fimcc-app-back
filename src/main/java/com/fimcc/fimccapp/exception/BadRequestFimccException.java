package com.fimcc.fimccapp.exception;

public class BadRequestFimccException extends FimccException {

    public BadRequestFimccException(final String message, final Throwable cause) {
        super(ErrorCode.BAD_REQUEST, message, cause);
    }

    public BadRequestFimccException(final Throwable cause) {
        super(ErrorCode.BAD_REQUEST, cause);
    }

    public BadRequestFimccException(final String message) {
        super(ErrorCode.BAD_REQUEST, message);
    }

    public BadRequestFimccException() {
        super(ErrorCode.BAD_REQUEST);
    }

    public BadRequestFimccException(final ErrorCode errorCode, final String message, final Throwable cause) {
        super(errorCode, message, cause);
    }

    public BadRequestFimccException(final ErrorCode errorCode, final Throwable cause) {
        super(errorCode, cause);
    }

    public BadRequestFimccException(final ErrorCode errorCode, final String message) {
        super(errorCode, message);
    }

    public BadRequestFimccException(final ErrorCode errorCode) {
        super(errorCode);
    }

}
