package com.fimcc.fimccapp.exception;

public class AccessDeniedFimccException extends BadRequestFimccException {

    public AccessDeniedFimccException(final String message, final Throwable cause) {
        super(ErrorCode.ACCESS_DENIED, message, cause);
    }

    public AccessDeniedFimccException(final Throwable cause) {
        super(ErrorCode.ACCESS_DENIED, cause);
    }

    public AccessDeniedFimccException(final String message) {
        super(ErrorCode.ACCESS_DENIED, message);
    }

    public AccessDeniedFimccException() {
        super(ErrorCode.ACCESS_DENIED);
    }

}
