package com.fimcc.fimccapp.exception;

import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletResponse;

public enum ErrorCode {

    BAD_CREDENTIALS(401100, HttpStatus.UNAUTHORIZED, "The username and password dos not match."),
    EXPIRED_CREDENTIALS(401200, HttpStatus.UNAUTHORIZED, "The credentials had expired."),
    BAD_REQUEST(400100, HttpStatus.BAD_REQUEST, "Bad request."),
    AMAZON_EMPTY_PARAMS(400200, HttpStatus.BAD_REQUEST, "Amazon empty params."),
    ACCESS_DENIED(401300, HttpStatus.UNAUTHORIZED, "Access denied."),
    INTERNAL_ERROR(500100, HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error."),
    WRITING_RESPONSE_ERROR(500200, HttpStatus.INTERNAL_SERVER_ERROR, "Error writing in response.");

    private final int fimccErrorCode;
    private final String message;
    private final HttpStatus httpStatus;

    ErrorCode(final int code, final String message){
        this.fimccErrorCode = code;
        this.message = message;
        this.httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    }

    ErrorCode(final int code, final HttpStatus httpStatus, final String message){
        this.fimccErrorCode = code;
        this.message = message;
        this.httpStatus = httpStatus;
    }

    public int getFimccErrorCode() {
        return fimccErrorCode;
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    @Override
    public String toString() {
        return "Http error fimccErrorCode: " + this.httpStatus + "\n"
            + "Application error fimccErrorCode: " + this.fimccErrorCode + "\n"
            + "Error message: " + this.message;
    }
}
