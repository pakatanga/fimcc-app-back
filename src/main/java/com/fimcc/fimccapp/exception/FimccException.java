package com.fimcc.fimccapp.exception;

public class FimccException extends Exception {

    private final ErrorCode errorCode;

    public FimccException(final ErrorCode errorCode, final String message, final Throwable cause) {
        super(errorCode.getMessage() + ": " + message, cause);
        this.errorCode = errorCode;
    }

    public FimccException(final ErrorCode errorCode, final Throwable cause) {
        super(errorCode.getMessage(), cause);
        this.errorCode = errorCode;
    }

    public FimccException(final ErrorCode errorCode, final String message) {
        super(errorCode.getMessage() + ": " + message);
        this.errorCode = errorCode;
    }

    public FimccException(final ErrorCode errorCode) {
        super(errorCode.getMessage());
        this.errorCode = errorCode;
    }
    public ErrorCode getErrorCode() {
        return errorCode;
    }

    @Override
    public String toString() {
        return this.errorCode.toString() + "\n"
            + this.getMessage();
    }
}
