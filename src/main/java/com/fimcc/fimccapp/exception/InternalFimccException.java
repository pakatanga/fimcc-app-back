package com.fimcc.fimccapp.exception;

public class InternalFimccException extends BadRequestFimccException {

    public InternalFimccException() {
        super(ErrorCode.INTERNAL_ERROR, "Internal server error.");
    }

    public InternalFimccException(final String message) {
        super(ErrorCode.INTERNAL_ERROR, message);
    }

    public InternalFimccException(final ErrorCode errorCode, final String message) {
        super(errorCode, message);
    }
}
