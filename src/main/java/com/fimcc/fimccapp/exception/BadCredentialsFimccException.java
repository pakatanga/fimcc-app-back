package com.fimcc.fimccapp.exception;

public class BadCredentialsFimccException extends BadRequestFimccException {

    public BadCredentialsFimccException(final String message, final Throwable cause) {
        super(ErrorCode.BAD_CREDENTIALS, message, cause);
    }

    public BadCredentialsFimccException(final Throwable cause) {
        super(ErrorCode.BAD_CREDENTIALS, cause);
    }

    public BadCredentialsFimccException(final String message) {
        super(ErrorCode.BAD_CREDENTIALS, message);
    }

    public BadCredentialsFimccException() {
        super(ErrorCode.BAD_CREDENTIALS);
    }

}
