package com.fimcc.fimccapp.exception;

public class ExpiredCredentialsFimccException extends BadRequestFimccException {

    public ExpiredCredentialsFimccException(final String message, final Throwable cause) {
        super(ErrorCode.EXPIRED_CREDENTIALS, message, cause);
    }

    public ExpiredCredentialsFimccException(final Throwable cause) {
        super(ErrorCode.EXPIRED_CREDENTIALS, cause);
    }

    public ExpiredCredentialsFimccException(final String message) {
        super(ErrorCode.EXPIRED_CREDENTIALS, message);
    }

    public ExpiredCredentialsFimccException() {
        super(ErrorCode.EXPIRED_CREDENTIALS);
    }

}
