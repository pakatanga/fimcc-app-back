package com.fimcc.fimccapp.exception;

public class FimccExceptionDTO {

    private final int fimccErrorCode;
    private final String localizedMessage;

    public FimccExceptionDTO(FimccException ex) {
        fimccErrorCode = ex.getErrorCode().getFimccErrorCode();
        localizedMessage = ex.getLocalizedMessage();
    }

    public int getFimccErrorCode() {
        return fimccErrorCode;
    }

    public String getLocalizedMessage() {
        return localizedMessage;
    }
}
