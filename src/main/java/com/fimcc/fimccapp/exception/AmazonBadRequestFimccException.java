package com.fimcc.fimccapp.exception;

public class AmazonBadRequestFimccException extends BadRequestFimccException {

    public AmazonBadRequestFimccException(final String message, final Throwable cause) {
        super(ErrorCode.AMAZON_EMPTY_PARAMS, message, cause);
    }

    public AmazonBadRequestFimccException(final Throwable cause) {
        super(ErrorCode.AMAZON_EMPTY_PARAMS, cause);
    }

    public AmazonBadRequestFimccException(final String message) {
        super(ErrorCode.AMAZON_EMPTY_PARAMS, message);
    }

    public AmazonBadRequestFimccException() {
        super(ErrorCode.AMAZON_EMPTY_PARAMS);
    }

}
