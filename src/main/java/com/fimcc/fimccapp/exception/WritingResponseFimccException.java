package com.fimcc.fimccapp.exception;

public class WritingResponseFimccException extends InternalFimccException {

    public WritingResponseFimccException() {
        super(ErrorCode.WRITING_RESPONSE_ERROR, "Error writing in response.");
    }

}
