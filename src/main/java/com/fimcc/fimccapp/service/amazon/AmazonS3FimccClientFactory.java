package com.fimcc.fimccapp.service.amazon;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.fimcc.fimccapp.configuration.properties.AmazonS3Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AmazonS3FimccClientFactory {

    private final AmazonS3Properties amazonS3Properties;

    private static final Logger log = LogManager.getLogger(AmazonS3FimccClientFactory.class);

    @Autowired
    public AmazonS3FimccClientFactory(final AmazonS3Properties amazonS3Properties) {
        this.amazonS3Properties = amazonS3Properties;
    }

    AmazonS3Client getClient() {
        log.info("Retrieving a new Amazon s3 client.");
        return (AmazonS3Client) AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(
            new BasicAWSCredentials(amazonS3Properties.getAccessKey(), amazonS3Properties.getSecretAccessKey())))
            .withRegion(amazonS3Properties.getRegionName()).build();
    }

    String getBucketName(){
        log.trace("Retrieving Amazon s3 bucket name.");
        return amazonS3Properties.getBucketName();
    }
}
