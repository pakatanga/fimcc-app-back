package com.fimcc.fimccapp.service.amazon;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.fimcc.fimccapp.domain.amazon.dto.UploadFileDTO;
import com.fimcc.fimccapp.exception.AmazonBadRequestFimccException;
import com.fimcc.fimccapp.exception.BadRequestFimccException;
import com.fimcc.fimccapp.exception.InternalFimccException;
import com.fimcc.fimccapp.exception.WritingResponseFimccException;
import com.fimcc.fimccapp.utils.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@Service
public class AmazonS3FileService {

    private static final Logger log = LogManager.getLogger(AmazonS3FileService.class);

    private final AmazonS3Client amazonS3Client;

    private final String bucketName;

    public AmazonS3FileService(final AmazonS3FimccClientFactory amazonS3FimccClientFactory) {
        this.amazonS3Client = amazonS3FimccClientFactory.getClient();
        this.bucketName = amazonS3FimccClientFactory.getBucketName();
    }

    public PutObjectResult uploadFile(UploadFileDTO uploadFileDTO) throws BadRequestFimccException {
        if (uploadFileDTO == null || uploadFileDTO.getFile() == null || uploadFileDTO.getFolderName() == null || uploadFileDTO.getFile() == null) {
            throw new AmazonBadRequestFimccException();
        }
        String finalRoute = StringUtils.buildAmazonPath(uploadFileDTO.getFolderName(), uploadFileDTO.getFileName());
        return amazonS3Client.putObject(bucketName, finalRoute, new ByteArrayInputStream(uploadFileDTO.getFile()), new ObjectMetadata());
    }

    public void deleteFile(String fileName) throws BadRequestFimccException {
        if (StringUtils.isEmpty(fileName)) {
            throw new AmazonBadRequestFimccException();
        }
        amazonS3Client.deleteObject(bucketName, fileName);
    }

    public void viewFile(String fileName, HttpServletResponse response) throws BadRequestFimccException {
        if (StringUtils.isEmpty(fileName)) {
            throw new AmazonBadRequestFimccException();
        }
        try (InputStream inputStream = retrieveInputStream(fileName); ServletOutputStream outputStream = response.getOutputStream()) {
            writeInputStream(inputStream, outputStream);
        } catch (IOException e) {
            throw new WritingResponseFimccException();
        }
    }

    private InputStream retrieveInputStream(String fileName) throws BadRequestFimccException {
        S3Object object = amazonS3Client.getObject(bucketName, fileName);
        if (object == null || object.getObjectContent() == null) {
            throw new AmazonBadRequestFimccException("Empty file: " + fileName);
        }
        return object.getObjectContent();
    }

    private void writeInputStream(InputStream inputStream, OutputStream outputStream) throws IOException{
        IOUtils.copy(inputStream, outputStream);
    }

    public void renameFile(String oldFileName, String newFileName) throws AmazonBadRequestFimccException {
        if (StringUtils.isEmpty(oldFileName) || StringUtils.isEmpty(newFileName)) {
            throw new AmazonBadRequestFimccException();
        }
        amazonS3Client.copyObject(bucketName, oldFileName, bucketName, newFileName);
        amazonS3Client.deleteObject(bucketName, oldFileName);
    }
}
