package com.fimcc.fimccapp.service.amazon;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import com.fimcc.fimccapp.exception.AmazonBadRequestFimccException;
import com.fimcc.fimccapp.exception.BadRequestFimccException;
import com.fimcc.fimccapp.utils.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class AmazonS3FolderService {

    private static final Logger log = LogManager.getLogger(AmazonS3FolderService.class);

    private final AmazonS3Client amazonS3Client;

    private final String bucketName;

    public AmazonS3FolderService(final AmazonS3FimccClientFactory amazonS3FimccClientFactory) {
        this.amazonS3Client = amazonS3FimccClientFactory.getClient();
        this.bucketName = amazonS3FimccClientFactory.getBucketName();
    }

    public ObjectListing listFolder(final String folderName) throws BadRequestFimccException {
        if (StringUtils.isBlank(folderName)) {
            throw new AmazonBadRequestFimccException(AmazonS3Options.EMPTY_FOLDER);
        }
        log.trace("Listing folder {}.", folderName);
        ListObjectsRequest listObjectsRequest = new ListObjectsRequest(bucketName, folderName + AmazonS3Options.FOLDER_CHAR, null, AmazonS3Options.FOLDER_CHAR, null);
        ObjectListing objectListing = amazonS3Client.listObjects(listObjectsRequest);
        log.trace("Folder {} listed with {} folders and {} objects",
            folderName,
            CollectionUtils.size(objectListing.getCommonPrefixes()),
            CollectionUtils.size(objectListing.getObjectSummaries()));
        return objectListing;
    }

    public PutObjectResult createFolder(final String folderName) throws BadRequestFimccException {
        if (StringUtils.isBlank(folderName)) {
            throw new AmazonBadRequestFimccException(AmazonS3Options.EMPTY_FOLDER);
        }
        log.trace("Creating folder {}.", folderName);
        PutObjectResult putObjectResult =
            amazonS3Client.putObject(bucketName, folderName + AmazonS3Options.FOLDER_CHAR, folderName);
        log.trace("Folder {} created.", folderName);
        return putObjectResult;
    }

    public void renameFolder(final String oldFolderName, final String newFolderName)
        throws BadRequestFimccException {
        if (StringUtils.isBlank(oldFolderName) || StringUtils.isBlank(newFolderName)) {
            throw new AmazonBadRequestFimccException(AmazonS3Options.EMPTY_FOLDER);
        }
        log.trace("Renaming folder from {} to {}.", oldFolderName, newFolderName);
        createFolder(newFolderName);
        ObjectListing objects = amazonS3Client.listObjects(bucketName, oldFolderName + AmazonS3Options.FOLDER_CHAR);
        int objectsDeleted = 0;
        do {
            for (S3ObjectSummary objectSummary : objects.getObjectSummaries()) {
                String newObjectKey = objectSummary.getKey().replace(oldFolderName, newFolderName);
                amazonS3Client.copyObject(bucketName, objectSummary.getKey(), bucketName, newObjectKey);
                amazonS3Client.deleteObject(bucketName, objectSummary.getKey());
                objectsDeleted++;
            }
            objects = amazonS3Client.listNextBatchOfObjects(objects);
        } while (objects.isTruncated());
        log.trace("Folder {} renamed to. {} objects moved", oldFolderName, newFolderName, objectsDeleted);
    }

    public void deleteFolder(final String folderName)  throws BadRequestFimccException {
        if (StringUtils.isBlank(folderName)) {
            throw new AmazonBadRequestFimccException(AmazonS3Options.EMPTY_FOLDER);
        }
        log.trace("Deleting folder {}.", folderName);
        ObjectListing objects = amazonS3Client.listObjects(bucketName, folderName + AmazonS3Options.FOLDER_CHAR);
        int objectsDeleted = 0;
        do {
            for (S3ObjectSummary objectSummary : objects.getObjectSummaries()) {
                amazonS3Client.deleteObject(bucketName, objectSummary.getKey());
                objectsDeleted++;
            }
            objects = amazonS3Client.listNextBatchOfObjects(objects);
        } while (objects.isTruncated());
        log.trace("Folder {} eliminated. {} objects deleted", folderName, objectsDeleted);
    }

}
