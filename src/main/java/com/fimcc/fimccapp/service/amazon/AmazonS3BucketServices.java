package com.fimcc.fimccapp.service.amazon;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.fimcc.fimccapp.utils.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class AmazonS3BucketServices {

    private static final Logger log = LogManager.getLogger(AmazonS3BucketServices.class);

    private final AmazonS3Client amazonS3Client;

    private final String bucketName;

    public AmazonS3BucketServices(final AmazonS3FimccClientFactory amazonS3FimccClientFactory) {
        this.amazonS3Client = amazonS3FimccClientFactory.getClient();
        this.bucketName = amazonS3FimccClientFactory.getBucketName();
    }

    public ObjectListing listBucket() {
        log.trace("Listing the entire bucket");
        ListObjectsRequest
            listObjectsRequest = new ListObjectsRequest(bucketName, null, null, AmazonS3Options.FOLDER_CHAR, null);
        ObjectListing objectListing = amazonS3Client.listObjects(listObjectsRequest);
        log.trace("Bucket listed with {} folders and {} objects",
            CollectionUtils.size(objectListing.getCommonPrefixes()),
            CollectionUtils.size(objectListing.getObjectSummaries()));
        return objectListing;
    }

}
