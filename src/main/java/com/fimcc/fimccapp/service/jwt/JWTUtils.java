package com.fimcc.fimccapp.service.jwt;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.fimcc.fimccapp.domain.Privilege;
import com.fimcc.fimccapp.domain.Role;
import com.fimcc.fimccapp.domain.User;
import com.fimcc.fimccapp.security.UserJWT;
import org.apache.catalina.security.SecurityUtil;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static com.fimcc.fimccapp.security.SecurityConstants.CLAIMS_AUTHORITIES_KEY;
import static com.fimcc.fimccapp.security.SecurityConstants.CLAIMS_AUTHORITIES_SEPARATOR;

class JWTUtils {

    static List<GrantedAuthority> convertPrivilegeCodesToAuthorities(final Collection<String> privileges) {
        return privileges.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }

    static String concatPrivilegesFromUser(final UserJWT user) {
        return user.getRoles().stream()
            .map(Role::getPrivileges)
            .flatMap(Collection::stream)
            .map(Privilege::getCode)
            .collect(Collectors.joining(CLAIMS_AUTHORITIES_SEPARATOR));
    }

    static List<String> extractPrivilegeStringListFromToken(final DecodedJWT decodedToken) {
        return Arrays.asList(decodedToken.getClaim(CLAIMS_AUTHORITIES_KEY).asString().split(CLAIMS_AUTHORITIES_SEPARATOR));
    }

    static List<GrantedAuthority> extractGrantedAuthoritiesFromToken(final DecodedJWT decodedToken) {
        return convertPrivilegeCodesToAuthorities(extractPrivilegeStringListFromToken(decodedToken));
    }

}
