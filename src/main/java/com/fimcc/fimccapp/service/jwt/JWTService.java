package com.fimcc.fimccapp.service.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fimcc.fimccapp.exception.BadCredentialsFimccException;
import com.fimcc.fimccapp.exception.BadRequestFimccException;
import com.fimcc.fimccapp.repository.UserJWTRepository;
import com.fimcc.fimccapp.security.UserJWT;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.fimcc.fimccapp.security.SecurityConstants.*;

@Service
public class JWTService {

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    private final UserJWTRepository userJWTRepository;

    public JWTService(final BCryptPasswordEncoder bCryptPasswordEncoder, final UserJWTRepository userJWTRepository) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.userJWTRepository = userJWTRepository;
    }

    public static DecodedJWT extractDecodedToken(final String token) {
        return JWT.require(Algorithm.HMAC512(SECRET.getBytes()))
                .build()
                .verify(token.replace(TOKEN_PREFIX, EMPTY_STRING));
    }

    public static UsernamePasswordAuthenticationToken createAuthenticationFromToken(final DecodedJWT decodedToken) {
        String username = decodedToken.getSubject();
        List<GrantedAuthority> authorities = JWTUtils.extractGrantedAuthoritiesFromToken(decodedToken);
        return new UsernamePasswordAuthenticationToken(username, null, authorities);
    }



    public String authenticate(final UserJWT userJWT) throws BadRequestFimccException {
        if (userJWT == null || StringUtils.isBlank(userJWT.getUsername()) || com.fimcc.fimccapp.utils.StringUtils.isEmpty(userJWT.getPassword())) {
            throw new BadRequestFimccException();
        }
        UserJWT user = userJWTRepository.findOneByUsername(userJWT.getUsername());
        boolean passwordMatches = bCryptPasswordEncoder.matches(userJWT.getPassword(), user.getPassword());
        if (!passwordMatches) {
            throw new BadCredentialsFimccException();
        }
        return createTokenFromUser(user);
    }

    private String createTokenFromUser(final UserJWT user) {
        String stringedPrivileges = JWTUtils.concatPrivilegesFromUser(user);
        return JWT.create().withSubject(user.getUsername()).withClaim(CLAIMS_AUTHORITIES_KEY, stringedPrivileges).sign(Algorithm.HMAC512(SECRET.getBytes()));
    }

}
