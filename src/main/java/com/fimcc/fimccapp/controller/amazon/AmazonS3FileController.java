package com.fimcc.fimccapp.controller.amazon;

import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.fimcc.fimccapp.domain.amazon.dto.UploadFileDTO;
import com.fimcc.fimccapp.exception.BadRequestFimccException;
import com.fimcc.fimccapp.service.amazon.AmazonS3FileService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api/s3")
public class AmazonS3FileController {

    private static final Logger log = LogManager.getLogger(AmazonS3FolderController.class);

    private final AmazonS3FileService amazonS3FileService;

    public AmazonS3FileController(final AmazonS3FileService amazonS3FolderServices) {
        this.amazonS3FileService = amazonS3FolderServices;
    }

    @PostMapping("/file")
    public PutObjectResult uploadFile(@RequestBody final UploadFileDTO uploadFileDTO) throws BadRequestFimccException {
        log.trace("Preparing to upload file {}.", uploadFileDTO);
        return amazonS3FileService.uploadFile(uploadFileDTO);
    }

    @DeleteMapping("/file")
    public void deleteFile(@RequestBody final String fileName) throws BadRequestFimccException {
        log.trace("Preparing to delete file {}.", fileName);
        amazonS3FileService.deleteFile(fileName);
    }

    @GetMapping("/file")
    public void getFile(@RequestParam(name = "fileName", required = false) final String fileName, HttpServletResponse response) throws BadRequestFimccException {
        log.trace("Preparing to retrieve file {}.", fileName);
        amazonS3FileService.viewFile(fileName, response);
    }

    @PutMapping("/file")
    public void renameFile(@RequestParam(name = "oldFileName", required = false) final String oldFileName,
        @RequestParam(name = "newFileName", required = false) final String newFileName) throws
        BadRequestFimccException {
        log.trace("Preparing to rename file {] into {}", oldFileName, newFileName);
        amazonS3FileService.renameFile(oldFileName, newFileName);
    }
}
