package com.fimcc.fimccapp.controller.amazon;

import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.fimcc.fimccapp.exception.BadRequestFimccException;
import com.fimcc.fimccapp.service.amazon.AmazonS3FolderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/s3")
public class AmazonS3FolderController {

    private static final Logger log = LogManager.getLogger(AmazonS3FolderController.class);

    private final AmazonS3FolderService amazonS3FolderServices;

    public AmazonS3FolderController(final AmazonS3FolderService amazonS3FolderServices) {
        this.amazonS3FolderServices = amazonS3FolderServices;
    }

    @PostMapping("/folder")
    public PutObjectResult createFolder(@RequestBody final String folderName) throws BadRequestFimccException {
        log.trace("Preparing to create folder {}.", folderName);
        return amazonS3FolderServices.createFolder(folderName);
    }

    @DeleteMapping("/folder")
    public void deleteFolder(@RequestBody final String folderName) throws BadRequestFimccException {
        log.trace("Preparing to delete folder {}.", folderName);
        amazonS3FolderServices.deleteFolder(folderName);
    }

    @GetMapping("/folder")
    public ObjectListing listFolder(@RequestParam(name = "folderName", required = false) final String folderName) throws BadRequestFimccException {
        log.trace("Preparing to list folder {}.", folderName);
        return amazonS3FolderServices.listFolder(folderName);
    }

    @PutMapping("/folder")
    public void renameFolder(@RequestParam(name = "oldFolderName", required = false) final String oldFolderName,
        @RequestParam(name = "newFolderName", required = false) final String newFolderName) throws
        BadRequestFimccException {
        log.trace("Preparing to rename folder {] into {}", oldFolderName, newFolderName);
        amazonS3FolderServices.renameFolder(oldFolderName, newFolderName);
    }

}
