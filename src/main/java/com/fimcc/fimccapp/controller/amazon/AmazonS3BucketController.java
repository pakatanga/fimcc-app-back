package com.fimcc.fimccapp.controller.amazon;

import com.amazonaws.services.s3.model.ObjectListing;
import com.fimcc.fimccapp.service.amazon.AmazonS3BucketServices;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/s3")
public class AmazonS3BucketController {

    private static final Logger log = LogManager.getLogger(AmazonS3FolderController.class);

    private final AmazonS3BucketServices amazonS3BucketServices;

    public AmazonS3BucketController(final AmazonS3BucketServices amazonS3BucketServices) {
        this.amazonS3BucketServices = amazonS3BucketServices;
    }

    @GetMapping("/bucket")
    public ObjectListing listBucket() {
        log.trace("Preparing to list entire bucket.");
        return amazonS3BucketServices.listBucket();
    }

}
