package com.fimcc.fimccapp.controller;

import com.fimcc.fimccapp.domain.User;
import com.fimcc.fimccapp.repository.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.internal.guava.Lists;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {

    private static final Logger log = LogManager.getLogger(JWTController.class);

    private final UserRepository userService;

    public UserController(UserRepository userService) {
        this.userService = userService;
    }

    @GetMapping("/user")
//    @PreAuthorize("hasAuthority('ABC')")
    public List<User> users() {
        log.trace("Listing users");
        try {
            return Lists.newArrayList(userService.findAll());
        } catch (Exception e) {
            log.error("Error listing users {}", e);
        }
        return null;
    }

    @GetMapping("/user/{username}")
//    @PreAuthorize("hasAuthority('DCE')")
    public User users(@PathVariable(value="username") String username) {
        return userService.findOneByUsername(username);
    }
}
