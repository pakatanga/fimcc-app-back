package com.fimcc.fimccapp.controller;

import com.fimcc.fimccapp.exception.BadRequestFimccException;
import com.fimcc.fimccapp.security.UserJWT;
import com.fimcc.fimccapp.service.jwt.JWTService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class JWTController {

    private static final Logger log = LogManager.getLogger(JWTController.class);

    private final JWTService jwtService;

    public JWTController(JWTService jwtService) {
        this.jwtService = jwtService;
    }

    @PostMapping("/authenticate")
    public String authenticate(@RequestBody UserJWT userJWT) throws BadRequestFimccException {
        return jwtService.authenticate(userJWT);
    }
}
