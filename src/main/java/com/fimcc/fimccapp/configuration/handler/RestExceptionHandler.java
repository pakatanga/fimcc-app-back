package com.fimcc.fimccapp.configuration.handler;

import com.fimcc.fimccapp.exception.BadRequestFimccException;
import com.fimcc.fimccapp.exception.FimccException;
import com.fimcc.fimccapp.exception.FimccExceptionDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger log = LogManager.getLogger(RestExceptionHandler.class);

    @ExceptionHandler(value
        = { BadRequestFimccException.class })
    protected ResponseEntity<Object> handleConflict(
        FimccException ex, WebRequest request) {
        log.error(ex.getMessage());
        String bodyOfResponse = "This should be application specific";
        return handleExceptionInternal(ex, new FimccExceptionDTO(ex),
            new HttpHeaders(), ex.getErrorCode().getHttpStatus(), request);
    }

}
