package com.fimcc.fimccapp.configuration.handler;

import com.fimcc.fimccapp.exception.AccessDeniedFimccException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestAccessDeniedExceptionHandler {

    @GetMapping("/access-denied")
    public String accessDenied() throws AccessDeniedFimccException {
        throw new AccessDeniedFimccException();
    }

}
