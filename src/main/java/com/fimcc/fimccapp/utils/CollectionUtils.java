package com.fimcc.fimccapp.utils;

import java.util.Collection;
import java.util.Map;

public class CollectionUtils extends org.springframework.util.CollectionUtils {

    public static Integer size(Collection collection) {
        if (isEmpty(collection)) {
            return 0;
        }
        return collection.size();
    }

    public static Integer size(Map map) {
        if (isEmpty(map)) {
            return 0;
        }
        return map.size();
    }
}
