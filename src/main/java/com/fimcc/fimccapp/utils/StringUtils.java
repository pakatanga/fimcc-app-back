package com.fimcc.fimccapp.utils;

import com.fimcc.fimccapp.service.amazon.AmazonS3Options;

public class StringUtils extends org.apache.commons.lang3.StringUtils {

    public static String buildAmazonPath(String... strings) {
        return join(strings, AmazonS3Options.FOLDER_CHAR);
    }

}
